#include <Wire.h>
#define I2C

void setup() {
   
    Wire.begin(I2C);
    Wire.onRequest(sendData); 
}

void loop() {
}

char datos[] = "hola";
int contador = 0;

void sendData() { 
    Wire.write(datos[contador]);
    ++contador;
    if (contador >= 5) {
         contador = 0;
    }
 }
