#include "DHT.h" //Libreria
#define DHT11PIN 7          // PIN digital 7 de arduino
#define DHT11TYPE DHT11     // Tipo de sensor
DHT dht11(DHT11PIN, DHT11TYPE);

void setup() {
  Serial.begin(9600); //Inicar comunicacion
  dht11.begin();
}

void loop() {
 
  float t11 = dht11.readTemperature();
  
  Serial.print("Temperatura = ");
  Serial.print(t11);
  Serial.println("°Celsius");
